var express = require('express');
var router = express.Router();

var home = require('../controllers/homeFibonacci');

    /* GET home page. */
router.get('/',home.index);
router.get('/getFibonacchi',home.getJesonFibo);


module.exports = router;
